using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ad_Example : MonoBehaviour {

    // Start is called before the first frame update
    void Start () {
        AdManager.Instance.OnRewardedCompleted += OnRewardedCompleted;

        AdManager.Instance.LoadInterstiatial ();
        AdManager.Instance.LoadBanner ();
        AdManager.Instance.LoadRewarded ();

        AdManager.Instance.ShowBanner ();
    }

    private void OnDestroy () {
        AdManager.Instance.OnRewardedCompleted -= OnRewardedCompleted;
    }
    // Update is called once per frame
    void Update () {
        if ( Input.GetMouseButtonDown ( 0 ) ) {
            AdManager.Instance.HideBanner ();
            AdManager.Instance.ShowInterstitial ( true );
        }
    }

    void OnRewardedCompleted () {
        Debug.Log ( "Récompense" );
    }
}
