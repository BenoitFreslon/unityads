using System;
using System.Collections;
using System.Collections.Generic;

#if UNITY_IOS || UNITY_EDITOR
using Unity.Advertisement.IosSupport;
using UnityEngine.iOS;
#endif

using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;

// Unity Ads 4.0+
// https://docs.unity3d.com/Packages/com.unity.ads@4.0/api/UnityEngine.Advertisements.Advertisement.html
// ATTrackingStatusBiding + Sample
// https://docs.unity3d.com/Packages/com.unity.ads.ios-support@1.2/api/Unity.Advertisement.IosSupport.ATTrackingStatusBinding.html

public class AdManager : MonoBehaviour, IUnityAdsInitializationListener, IUnityAdsLoadListener, IUnityAdsShowListener {

    static public AdManager Instance;

    [SerializeField] bool testMode = false;

    // Délais en secondes
    [SerializeField] float delayBetweenAds = 120;
    [SerializeField] BannerPosition bannerPosition = BannerPosition.BOTTOM_CENTER;


    [Header ( "Android" )]
    [SerializeField] string gameId_Android = "0000000";
    [SerializeField] string bannerId_Android = "Banner_Android";
    [SerializeField] string rewardedId_Android = "Rewarded_Android";
    [SerializeField] string interstitialId_Android = "Interstitial_Android";

    [Header ( "iOS" )]
    [SerializeField] string gameId_iOS = "0000000";
    [SerializeField] string bannerId_iOS = "Banner_iOS";
    [SerializeField] string rewardedId_iOS = "Rewarded_iOS";
    [SerializeField] string interstitialId_iOS = "Interstitial_iOS";

    string gameId;
    string bannerId;
    string rewardedId;
    string interstitialId;

    public UnityAction OnRewardedCompleted;

    float timeLastAd = 0;


    void Awake () {

        if ( Instance != null && Instance != this )
            Destroy ( gameObject );
        Instance = this;
    }
    // Start is called before the first frame update
    void Start () {
        Debug.Log ( "AdManager Starts" );
        // Switch platforms
        if ( Application.platform == RuntimePlatform.Android ) {
            gameId = gameId_Android;
            bannerId = bannerId_Android;
            rewardedId = rewardedId_Android;
            interstitialId = interstitialId_Android;
        } else {
            gameId = gameId_iOS;
            bannerId = bannerId_iOS;
            rewardedId = rewardedId_iOS;
            interstitialId = interstitialId_iOS;
        }

        RequestTrackingAuthorization ();

    }

    void Initialize () {
        Debug.Log ( "Initialize" );
        Advertisement.Initialize ( gameId, testMode, this );

        Advertisement.Banner.SetPosition ( bannerPosition );

        timeLastAd = Time.realtimeSinceStartup;
    }

    public void RequestTrackingAuthorization () {
        Debug.Log ( "RequestTrackingAuthorization" );
#if UNITY_IOS || UNITY_EDITOR

        if ( Application.platform == RuntimePlatform.IPhonePlayer ) {

            var status = ATTrackingStatusBinding.GetAuthorizationTrackingStatus ();
            Version currentVersion = new Version ( Device.systemVersion );
            Version ios14 = new Version ( "14.5" );

            if ( status == ATTrackingStatusBinding.AuthorizationTrackingStatus.NOT_DETERMINED && currentVersion >= ios14 ) {
                ATTrackingStatusBinding.RequestAuthorizationTracking ( AuthorizationTrackingReceived );
            } else {
                Initialize ();
            }
        } else {
            Initialize ();
        }
#else
        Initialize();
#endif
    }

    private void AuthorizationTrackingReceived ( int status ) {
        Debug.LogFormat ( "Tracking status received: {0}", status );
        Initialize ();
    }

    // GDPR
    public void SetConsent ( bool consent ) {
        // If the user opts in to targeted advertising:
        MetaData gdprMetaData = new MetaData ( "gdpr" );
        gdprMetaData.Set ( "consent", consent ? "true" : "false" );
        Advertisement.SetMetaData ( gdprMetaData );

    }

    public void LoadBanner () {
        Debug.Log ( "LoadBanner" );
        Advertisement.Banner.Load ( bannerId );
    }
    public void LoadInterstiatial () {
        Debug.Log ( "LoadInterstiatial" );
        Advertisement.Load ( interstitialId, this );
    }
    public void LoadRewarded () {
        Debug.Log ( "LoadRewarded" );
        Advertisement.Load ( rewardedId, this );
    }

    public void ShowBanner () {
        Debug.Log ( "ShowBanner " + Advertisement.Banner.isLoaded );
        if ( !Advertisement.Banner.isLoaded ) {
            LoadBanner ();
            return;
        }
        Advertisement.Banner.Show ( bannerId );
    }

    public void ShowInterstitial ( bool force = false ) {
        Debug.Log ( "ShowInterstitial" );
        if ( !force ) {
            // Ne pas afficher la pub si le délai n'est pas assez long
            if ( Time.realtimeSinceStartup - timeLastAd < delayBetweenAds ) {
                Debug.Log ( "Ne pas afficher de pub maintenant, c'est trop tôt !" );
                return;
            }
        }
        HideBanner ();
        timeLastAd = Time.realtimeSinceStartup;
        Advertisement.Show ( interstitialId, this );
    }
    public void ShowRewarded () {
        Debug.Log ( "ShowRewarded" );
        HideBanner ();
        Advertisement.Show ( rewardedId, this );
    }

    public void HideBanner () {
        Advertisement.Banner.Hide ();
    }

    // IUnityAdsInitializationListener
    public void OnInitializationComplete () {
        Debug.Log ( "OnInitializationComplete" );
    }

    public void OnInitializationFailed ( UnityAdsInitializationError error, string message ) {
        Debug.Log ( "UnityAdsInitializationError: " + error + " " + message );
    }

    // IUnityAdsLoadListener
    public void OnUnityAdsAdLoaded ( string placementId ) {
        Debug.Log ( "OnUnityAdsAdLoaded: " + placementId );
    }

    public void OnUnityAdsFailedToLoad ( string placementId, UnityAdsLoadError error, string message ) {
        Debug.Log ( "OnUnityAdsFailedToLoad: " + placementId + " " + error + " " + message );
    }

    // IUnityAdsShowListener
    public void OnUnityAdsShowFailure ( string placementId, UnityAdsShowError error, string message ) {
        Debug.Log ( "OnUnityAdsShowFailure: " + placementId + " " + error + " " + message );
    }

    public void OnUnityAdsShowStart ( string placementId ) {
        Debug.Log ( "OnUnityAdsShowStart: " + placementId );
    }

    public void OnUnityAdsShowClick ( string placementId ) {
        Debug.Log ( "OnUnityAdsShowClick: " + placementId );
    }

    public void OnUnityAdsShowComplete ( string placementId, UnityAdsShowCompletionState showCompletionState ) {
        Debug.Log ( "OnUnityAdsShowComplete: " + placementId + " " + showCompletionState );
        if ( placementId == rewardedId ) {
            // Récompenser le joueur ici
            if ( OnRewardedCompleted != null )
                OnRewardedCompleted.Invoke ();
        }
    }



}
